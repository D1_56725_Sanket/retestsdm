create table Movie(
    movie_id integer primary key auto_increment,
    movie_title varchar(100),
    movie_release_date varchar(100),
    movie_time varchar(50),
    director_name varchar(100)
);