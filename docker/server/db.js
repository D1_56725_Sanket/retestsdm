const mysql = require('mysql2')

const pool = mysql.createPool({
    host: 'mydb',
    user: 'root',
    password: 'root',
    database: 'movie',
    port: 3306,
    waitForConnections: true,
    connectionLimit: 10,
    queryLimit: 0
})

module.exports = pool